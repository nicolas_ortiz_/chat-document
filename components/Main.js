import React from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,Image, ImageBackground,
} from 'react-native';

class Main extends React.Component {
  static navigationOptions = {
    title: 'CHAT',
  };

  state = {
    name: '',
  };

  onPress = () =>
    this.props.navigation.navigate('Chat', { name: this.state.name });

  onChangeText = name => this.setState({ name });

  render() {
    return (
      <ImageBackground source={require('./UnaImagenMas/un_imagen_mas.png')} style={styles.container}>
    
      <View style={styles.viewContainer}>
        <Text style={styles.title}>Intresa tu correo:</Text>
        <TextInput
          style={styles.nameInput}
          placeHolder="nicolas ortiz"
          onChangeText={this.onChangeText}
          value={this.state.name}
        />
        <TouchableOpacity onPress={this.onPress}>
          <Text style={styles.title}>Ingresar</Text>
        </TouchableOpacity>
      </View>
      </ImageBackground>
    );
  }
}

const offset = 24;
const styles = StyleSheet.create({
  title: {
    height: "25%",
    fontSize: offset,
    color: '#FFFFFF',

  },
  nameInput: {
    height: offset * 2,
    borderColor:  '#FFFFFF',
    color: '#FFFFFF',
    margin: offset,
    paddingHorizontal: offset,
    borderWidth: 1,
  },
  buttonText: {
    color: '#FFFFFF',

    marginLeft: offset,
    fontSize: offset,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    width: "100%",
    height: "150%",
    flexWrap: 'wrap',
    color: '#FFFFFF',
  },

});

export default Main;
